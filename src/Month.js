import React from 'react';
import Day from './Day';

const Month = ({ selectedMonth }) => {
  const getFirstDayOfMonth = (year, month) => {
    return new Date(year, month, 1);
  };

  const renderFirstDay = (year, month) => {
    const firstDay = getFirstDayOfMonth(year, month);
    return <Day date={firstDay} />;
  };

  return (
    <div>
      <h2>{selectedMonth}</h2>
      <div>{renderFirstDay(2021, selectedMonth - 1)}</div>
    </div>
  );
};

export default Month;

