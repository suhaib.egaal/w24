import React from 'react';

const Day = ({ date }) => {
  const daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  const dayIndex = new Date(date).getDay();
  return <span>{daysOfWeek[dayIndex]}</span>;
};

export default Day;
