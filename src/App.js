import React, { useState } from 'react';
import Month from './Month';

const App = () => {
 
  const [selectedMonth, setSelectedMonth] = useState(1);

 
  const handleMonthChange = (event) => {
    setSelectedMonth(parseInt(event.target.value, 10));
  };

  return (
    <div>
      <h1>Month Selector App</h1>
      
      <label htmlFor="monthSelector">Select a month:</label>
      <select id="monthSelector" onChange={handleMonthChange} value={selectedMonth}>
        {Array.from({ length: 12 }, (_, i) => i + 1).map((month) => (
          <option key={month} value={month}>
            {month}
          </option>
        ))}
      </select>
      
      <Month selectedMonth={selectedMonth} />
    </div>
  );
};

export default App;

